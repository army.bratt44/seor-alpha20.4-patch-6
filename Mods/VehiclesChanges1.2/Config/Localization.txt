Key,Source,Context,Changes,English,French,German,Klingon,Spanish,Polish

mediumEngine,items,Gun,New,Medium Engine,,,,,
mediumEngineDesc,items,Gun,New,Medium V2 Engine used in Motorcycles and Gyrocopters,,,,,

largeEngine,items,Gun,New,Large Engine,,,,,
largeEngineDesc,items,Gun,New,Large V4 Engine used in the 4x4 Truck ,,,,,

vehicleTire,items,Gun,New,Vehicle Tire,,,,,
vehicleTireDesc,items,Gun,New,Rubber Tire for Motorized Vehicles, combine it with a Rim to make a Wheel ,,,,,

vehicleRim,items,Gun,New,Vehicle Rim,,,,,
vehicleRimDesc,items,Gun,New,Iron Rim for Motorized Vehicles, combine it with a Tire to make a Wheel,,,,,

vehicleBicycleWheel,items,Gun,New,Bicycle Wheel,,,,,
vehicleBicycleWheelDesc,items,Gun,New,Small Bicycle wheel ,,,,,

vehicleEngineParts,items,Gun,New,Engine Parts,,,,,
vehicleEnginePartsDesc,items,Gun,New,Parts harvested from broken engine but still in good shape\ncan be used to make new engines ,,,,,






