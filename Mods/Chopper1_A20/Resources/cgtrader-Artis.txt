Vehicle Name: Helicopter

3D Model Artists Name: irs1182@Sketchfab.com

Site Link-(https://sketchfab.com/3d-models/helicopter-91366449ce6842f784748a914e29d641)

All Models in this modlet are subject to creative commons licence.
https://creativecommons.org/licenses/by/4.0/


No Models have been altered from the 'original creator' they are set up with an extra overlay texture and appropriate lighting shader in unity for 7 Days to Die game.

(ActiniumTiger and Ragsy)
Thank You To All Artists who post Free Assets for Modders to use in games.
Much appreciated!!!!