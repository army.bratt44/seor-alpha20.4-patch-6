Vehicle Name: Three Cylinder Naked Street Bike  (https://sketchfab.com/3d-models/three-cylinder-naked-street-bike-0897a975845647919728095d117a6255) by Jamie Hamel-Smith is licensed under Creative Commons Attribution (http://creativecommons.org/licenses/by/4.0/).

3D Model Artists Name: Jamie Hamel-Smith 
Link-(https://skfb.ly/6TJ8F)

All Models in this modlet are subject to creative commons licence.
https://creativecommons.org/licenses/by/4.0/

No Models have been altered from the 'original creator' they are set up with appropriate lighting shader in unity for 7 Days to Die game.

(ActiniumTiger and Ragsy)
Thank You To All Artists who post Free Assets for Modders to use in games.
Much appreciated!!!!